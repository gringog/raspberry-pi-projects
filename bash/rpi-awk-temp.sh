#!/usr/bin/env bash

# oupputs the rpi temp in C 
awk '{printf("%.1f\n",$1/1e3)}' /sys/class/thermal/thermal_zone0/temp