# Mount the raspbmc share

sudo mount -t cifs -o user=pi,password=raspberry,rw,file_mode=0777,dir_mode=0777 //192.168.0.24/devices /mnt/raspbmc/

volume="/mnt/raspbmc"
if mount|grep $volume; then

    echo "Raspbmc is mounted"
	echo "Now loading the services"

	# Start SabNzbd - only needed for news group feeds and such
	sudo service sabnzbd start

	# Start couchpotato
	sudo service couchpotato start

	# Start the torrent manager and web gui as a background task
	deluged
	#deluge-web &

	# Start sickbeard
	sudo service sickbeard start
else
	echo "Raspbmc is not mounted"
fi


# Or install as fstab
# sudo nano /etc/fstab

# ==For Guest Login ==
# //WindowsPC/Share1 /mnt/mountfoldername cifs guest 0 0
# == For Password Protected Login==
# //WindowsPC/Share1 /mnt/mountfoldername cifs username=yourusername,password=yourpassword 0 0

# == to unmount ==
# sudo umount //WindowsPC/Share1