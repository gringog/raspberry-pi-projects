#!/usr/bin/env bash


# Installing Dependencies
sudo apt-get -y install python2.6 python-cheetah python-openssl par2
sudo sh -c "echo \"deb-src http://mirrordirector.raspbian.org/raspbian/ wheezy main contrib non-free rpi\" >> /etc/apt/sources.list"
sudo apt-get update
sudo apt-get -y build-dep unrar-nonfree
sudo apt-get source -b unrar-nonfree
sudo dpkg -i unrar*.deb

# SABnzdb
sudo addgroup nzb
# Ceate the user for SABnzbd:
sudo useradd --system --user-group --no-create-home --groups nzb sabnzbd
wget http://downloads.sourceforge.net/project/sabnzbdplus/sabnzbdplus/0.7.16/SABnzbd-0.7.16-src.tar.gz
tar xzf SABnzbd-0.7.16-src.tar.gz
sudo mv SABnzbd-0.7.16 /usr/local/sabnzbd
sudo chown -R sabnzbd:nzb /usr/local/sabnzbd
sudo mkdir /var/sabnzbd
sudo chown sabnzbd:nzb /var/sabnzbd

#SickBeard
sudo useradd --system --user-group --no-create-home sickbeard
git clone git://github.com/midgetspy/Sick-Beard.git
sudo mv Sick-Beard /usr/local/sickbeard
sudo chown -R sickbeard:nzb /usr/local/sickbeard
sudo chmod ug+rw /usr/local/sickbeard/autoProcessTV/
sudo mkdir /var/sickbeard
sudo chown sickbeard:nzb /var/sickbeard

#CouchPotato
sudo useradd --system --user-group --no-create-home couchpotato
git clone git://github.com/RuudBurger/CouchPotatoServer.git
sudo mv CouchPotatoServer /usr/local/couchpotato
sudo chown -R couchpotato:couchpotato /usr/local/couchpotato
sudo mkdir /var/couchpotato
sudo chown -R couchpotato:couchpotato /var/couchpotato


# Create Start up files
sudo cat > /etc/init.d/sabnzbd << EOF
#!/bin/sh
### BEGIN INIT INFO
# Provides:          SABnzbd
# Required-Start:    $network $remote_fs $syslog
# Required-Stop:     $network $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start SABnzd at boot time
# Description:       Start SABnzbd.
### END INIT INFO

case "$1" in
start)
  echo "Starting SABnzbd."
  /usr/bin/sudo -u sabnzbd -H /usr/local/sabnzbd/SABnzbd.py -d -f /var/sabnzbd/sabnzbd.ini
;;
stop)
  echo "Shutting down SABnzbd."
  p=`ps aux | grep -v grep | grep SABnzbd.py | tr -s \ | cut -d ' ' -f 2`
  if [ -n "$p" ]; then
    kill -2 $p > /dev/null
    while ps -p $p > /dev/null; do sleep 1; done
  fi
;;
*)
  echo "Usage: $0 {start|stop}"
  exit 1
esac
exit 0
EOF
sudo chmod 755 /etc/init.d/sabnzbd

sudo cat > /etc/init.d/sickbeard << EOF
#!/bin/sh
### BEGIN INIT INFO
# Provides:          SickBeard
# Required-Start:    $network $remote_fs $syslog
# Required-Stop:     $network $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start SickBeard at boot time
# Description:       Start SickBeard.
### END INIT INFO

case "$1" in
start)
  echo "Starting SickBeard."
  /usr/bin/sudo -u sickbeard -H /usr/local/sickbeard/SickBeard.py -d --datadir /var/sickbeard --config /var/sickbeard/sickbeard.ini
;;
stop)
  echo "Shutting down SickBeard."
  p=`ps aux | grep -v grep | grep SickBeard.py | tr -s \ | cut -d ' ' -f 2`
  if [ -n "$p" ]; then
    sb_api_key=`grep -m 1 api_key ${sb_config} | cut -d ' ' -f 3`;
    sb_port=`grep -m 1 web_port ${sb_config} | cut -d ' ' -f 3`;
    wget -q --delete-after http://localhost:${sb_port}/api/${sb_api_key}/\?cmd=sb.shutdown
    while ps -p $p > /dev/null; do sleep 1; done
  fi
;;
*)
  echo "Usage: $0 {start|stop}"
  exit 1
esac

exit 0
EOF
sudo chmod 755 /etc/init.d/sickbeard

sudo cat > /etc/init.d/couchpotato << EOF
#!/bin/sh
### BEGIN INIT INFO
# Provides: CouchPotato
# Required-Start: $network $remote_fs $syslog
# Required-Stop: $network $remote_fs $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Start CouchPotato at boot time
# Description: Start CouchPotato.
### END INIT INFO
case "$1" in
start)
 echo "Starting CouchPotato."
 /usr/bin/sudo -u couchpotato -H /usr/local/couchpotato/CouchPotato.py --daemon --data_dirdir=/var/couchpotato --config_file=/var/couchpotato/couchpotato.ini
;;
stop)
 echo "Shutting down CouchPotato."
  p=`ps aux | grep -v grep | grep CouchPotato.py | tr -s \ | cut -d ' ' -f 2`
  couch_api_key=`grep -m 1 api_key /var/couchpotato/couchpotato.ini | cut -d ' ' -f 3`;
  couch_port=`grep -m 1 port /var/couchpotato/couchpotato.ini | cut -d ' ' -f 3`;
 wget -q --delete-after http://localhost:${couch_port}/api/${couch_api_key}/app.shutdown
 while ps -p $p > /dev/null; do sleep 1; done
;;
*)
 echo "Usage: $0 {start|stop}"
 exit 1
esac
exit 0
EOF
sudo chmod 755 /etc/init.d/couchpotato


## Deluge Install
sudo apt-get install deluged deluge-console
# Run to create the config file
deluged
# Kill the process
sudo pkill deluged
# Copy the old file over and login with pi raspberry
cp ~/.config/deluge/auth ~/.config/deluge/auth.old
echo "pi:raspberry:10" > ~/.config/deluge/auth

# Now run 
#   deluged
#   deluge-console

# and type the following into the console
# 
# config -s allow_remote True
# config allow_remote
# exit



 
echo " == Installed and configured == "
echo " Please run the following commands to make sure the config files get generated"
echo "sudo su sabnzbd -c \"/usr/local/sabnzbd/SABnzbd.py -f /var/sabnzbd -s 192.168.0.11:8080\""
echo "sudo su sickbeard -c \"/usr/local/sickbeard/SickBeard.py --datadir /var/sickbeard --config /var/sickbeard/sickbeard.ini\""
echo "sudo su couchpotato -c \"/usr/local/couchpotato/CouchPotato.py --data_dir=/var/couchpotato --config_file=/var/couchpotato/couchpotato.ini\""
