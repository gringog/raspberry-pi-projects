# Install apache2
sudo apt-get install apache2 mysql-server mysql-client php5-mysql php-soap -fy

# Make the vhosts direcotry for sites
sudo mkdir /var/www/vhosts


#
# Edit the /etc/apache2/sites-enabled/000-default file with these
# settings under the <VirtualHost *:80> line
#
#   VirtualDocumentRoot /var/www/vhosts/%0/httpdocs
#   VirtualScriptAlias /var/www/vhosts/%0/cgi-bin
#
#
# Change the docment root to /var/www/vhosts
# 
#   DocumentRoot /var/www/vhosts
#
#
# To make .htaccess files work change AllowOverride none to 
# 
#   AllowOverride All
#
#
# After the </VirtualHost> you can set server enviroment variables
# such as setting a mysql username and password
#
#    SetEnv MYSQL_USERNAME username
#    SetEnv MYSQL_PASSWORD password
#


# Apache vhosts
sudo a2enmod vhost_alias

# ReWrite module
sudo a2enmod rewrite

sudo a2enmod speling
sudo a2enmod expires
sudo a2enmod filter
sudo a2enmod headers


sudo service apache2 restart
