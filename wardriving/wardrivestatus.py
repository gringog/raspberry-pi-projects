
import RPi.GPIO as GPIO
import os
import subprocess
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(3, GPIO.OUT)#Sets the GPIO pin to be an output pin
GPIO.setup(7, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.output(3, True) #Makes sure all LED's are at the same state (off)
GPIO.output(7, True)
GPIO.output(11, True)
while 1 < 2:
    kismet = subprocess.Popen(['ps -ef | grep kismet'], stdout=subprocess.PIPE, shell=True) #Assigns the output from the grep to the kismet variable
    (output, error) = kismet.communicate()
    if 'kismet_server' in output:
        GPIO.output(3, False) #Turn on YELLOW LED
    else:
        GPIO.output(3, True) #Turn off YELLOW LED
    gps = subprocess.Popen(['ps -ef | grep gpsd'], stdout=subprocess.PIPE, shell=True) #Assigns the output from the grep to the gps variable
    (output, error) = gps.communicate()
    if 'gpsd' in output:
        GPIO.output(7, False) #Turn on RED LED
    else:
        GPIO.output(7, True) #Turn off RED LED
        
        
# Source : http://www.youtube.com/watch?v=RVVaWoxHKJo        
# The two entries added to the  /etc/rc.local file are:
# python /scripts/wardrivestatus.py &
# kismet_server &
#
# Adding these entries will allow everything to start automatically on boot.
#
# The model number of GPS receiver is BU-353.
#
# The network card is an: Alfa networks AWUS036H

# Converting the kismetkml file to kml
# python pykismetkml-2010-02-R1.py -i loc/to/netxml/file
