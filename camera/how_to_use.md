How to use the Raspberry Pi camera software
===========================================

raspivid is a command line application that allows you to capture video
with the camera module, while the application raspistill allows you to 
capture images.

-o or –output specifies the output filename and -t or –timeout specifies 
the amount of time that the preview will be displayed in milliseconds. 
Note that this set to 5s by default and that raspistill will capture the
final frame of the preview period.

-d or –demo runs the demo mode that will cycle through the various image
effects that are available.

Example commands
----------------
Capture an image in jpeg format::

raspistill -o image.jpg

Capture a 5s video in h264 format::

raspivid -o video.h264

Capture a 10s video::

raspivid -o video.h264 -t 10000

Capture a 10s video in demo mode::

raspivid -o video.h264 -t 10000 -d

To see a list of possible options for running raspivid or raspistill, you can run::

raspivid | less

raspistill | less

Use the arrow keys to scroll and type q to exit.



How to stream video from the Raspberry Pi camera over a network
===============================================================

To view the feed on Linux
-------------------------
Install the dependencies by running the following in a terminal::

sudo apt-get install mplayer netcat

Find your IP address by running ifconfig. 
(Your IP address will be listed in the console output and will probably be of the form 192.168.1.XXX).

Run the following command in a terminal to view the feed using MPlayer::

nc -l -p 5001 | mplayer -fps 31 -cache 1024 -



To transmit the feed from the Pi with camera module attached
------------------------------------------------------------

After setting up the “receiving” machine as per the instructions above, 
run the following commands in a terminal on the “transmitting” Pi::

raspivid -t 999999 -o – | nc [insert the IP address of the client] 5001

You can then use the commands listed in the “How to use the Raspberry Pi camera software”
section above to change the capture time or to add a video effect.

