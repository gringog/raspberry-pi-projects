#!/usr/bin/env bash

# source : http://designspark.com/blog/time-lapse-photography-with-the-raspberry-pi-camera

DATESTAMP=$(date -u +"%d%m%Y_%H%M-%S")
# create a list of all the stills
ls  /var/tlcam/stills/*.jpg > /var/tlcam/stills_$DATESTAMP.txt

# The mencoder [1] software was then used to create a HD (1080p) MPEG-4 video file for upload to YouTube[2]
#
# [1] http://en.wikipedia.org/wiki/MEncoder
# [2] http://support.google.com/youtube/bin/answer.py?hl=en-GB&answer=1722171


mencoder -nosound -ovc lavc -lavcopts vcodec=mpeg4:aspect=16/9:vbitrate=8000000 -vf scale=1920:1080 -o /var/tlcam/tlcam_$DATESTAMP.avi -mf type=jpeg:fps=24 mf://@/var/tlcam/stills_$DATESTAMP.txt


