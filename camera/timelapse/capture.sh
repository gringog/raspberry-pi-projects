#!/bin/bash

# source : http://designspark.com/blog/time-lapse-photography-with-the-raspberry-pi-camera
# The command /opt/vc/bin/raspistill is used to capture still images and Andrew
# found that with the default options this took around 6 seconds to complete.

# Every time the Pi boots an init script reads the number stored in /var/tlcam/series and increments it by 1.
# The script is then executed and captures an image every ~10 seconds.

#5 MP
#WIDTH=2592
#HEIGHT=1944

#2.5MP
WIDTH=1920
HEIGHT=1080


ROLL=$(cat /var/tlcam/series)

SAVEDIR=/var/tlcam/net/stills

while [ true ]; do
    echo $(date -u +"%d%m%Y_%H%M-%S")
    filename=$ROLL-$(date -u +"%d%m%Y_%H%M-%S").jpg

    /opt/vc/bin/raspistill -o $SAVEDIR/$filename

    sleep 4;

done;

