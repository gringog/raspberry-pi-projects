# Captures a 20 second file with vertical flip
raspivid -t 20000 -vf -o test-vf.h264

# 2 min capture at 720HD
raspivid -w 720 -h 405 -vf -o 720x405.h264 -t 120000
