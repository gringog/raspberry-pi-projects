#!/usr/bin/env python

# To disable the red LED you simply need to add the following line to your /boot/config.txt file :
# disable_camera_led=1
# Source : http://www.raspberrypi-spy.co.uk/2013/05/how-to-disable-the-red-led-on-the-pi-camera-module/#more-1636

import time
import RPi.GPIO as GPIO
 
# Use GPIO numbering
GPIO.setmode(GPIO.BCM)
 
# Set GPIO for camera LED
CAMLED = 5
 
# Set GPIO to output
GPIO.setup(CAMLED, GPIO.OUT, initial=False) 
 
# Five iterations with half a second
# between on and off
for i in range(5):
  GPIO.output(CAMLED,True)  # On
  time.sleep(0.5)
  GPIO.output(CAMLED,False) # Off
  time.sleep(0.5)