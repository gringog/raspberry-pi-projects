#!/bin/bash

# autotweet
#
# This script sends a random tweet form a list every time it's called.
# Use:
#   autotweet           Send a random tweet from the list
#   autotweet 6         Send the 6th tweet from the list
#
# Author: San Bergmans
#         www.sbprojects.com
# Source : http://www.sbprojects.com/projects/raspberrypi/autotweet.php

# Configuration variables
TWITTER=/home/pi/bin/tweet
NUM_TWEETS=10

# Other variables
TWIT_URL="http://api.supertweet.net/1/statuses/update.xml"

# Pick a random number
SEED=$(head -c4 /dev/urandom | od -t u4 | awk '{ print $2 }')
RAND=$[ ( $SEED % $NUM_TWEETS + 1 ) ]

if [ $# -ne 0 ] ; then
    RAND=$1
fi

case $RAND in
    1) TWEET="Tweet number 1";;
    2) TWEET="Tweet number 2";;
    3) TWEET="Tweet number 3";;
    4) TWEET="Tweet number 4";;
    5) TWEET="Tweet number 5";;
    6) TWEET="Tweet number 6";;
    7) TWEET="Tweet number 7";;
    8) TWEET="Tweet number 8";;
    9) TWEET="Tweet number 9";;
   10) TWEET="Tweet number 10";;
    *) TWEET="Default tweet for illegal random numbers";;
esac

# Send the tweet
$TWITTER "$TWEET"