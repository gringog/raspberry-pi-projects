#! /bin/bash

# tweetmyip
#
# This script sends a tweet every time the network comes up.
# The tweet contains a time stamp and the obtained IP address.
#
# Use: tweetmyip [interfacename]
#
# Author: San Bergmans
#         www.sbprojects.com
# Source : http://www.sbprojects.com/projects/raspberrypi/tweetmyip.php

# Configuration variables
RPINAME="MyPi"
TWEET="/home/pi/bin/tweet"
SHOWPUBLICIP="no"

# Get interface name from parameters
if [ $# -eq 0 ]
then
        IFC="eth0"
else
        IFC="$1"
fi

ifconfig $IFC &> /dev/null
if [ $? -ne 0 ]
then
        exit 1
fi

# Get current private IP address for select interface
PRIVATE=$(ifconfig $IFC | grep "inet addr:" | awk '{ print $2 }')
PRIVATE=${PRIVATE:5}
IPV6=$(ifconfig $IFC | grep "Scope:Global") | awk '{ print $3 }' | awk -F/ '{ print $1 }'

# Exit if IP address is empty
if [ -z $PRIVATE ]
then
    exit 0
fi

# Wait about 2 minutes for the RTC to be set after boot (in steps of 10 seconds)
for I in {1..12}
do
    sleep 10
    if [ $(date +%Y) != "1970" ]
    then
        # Yes! The clock is set. Now it's time to send the tweet
                if [ $SHOWPUBLICIP = "yes" ]
        then
            PUBLIC=$(curl -s checkip.dyndns.org|sed -e 's/.*Current IP Address: //' -e 's/<.*$//')
            MSG="$(date +%F\ %T) $RPINAME $IFC now has IP-Address $PRIVATE. Its public address is $PUBLIC."
        else
            MSG="$(date +%F\ %T) $RPINAME $IFC now has IP-Address $PRIVATE"
        fi
        $TWEET "$MSG"
        exit 0
    fi
done