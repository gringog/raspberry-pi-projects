#!/bin/bash

# Installation 
# $sudo apt-get install sane sane-utils imagemagick

# Check if it's working
# $scanimage -L
 
# Setup a vaiable for the filename
FILENAME=`date +%s`.png
# Scan 

#echo 1 > /sys/class/gpio/gpio18/value # process initiated – let’s turn led RED on

sudo scanimage -l 0 -t 0 -x 215 -y 297 -p --resolution 300 | convert - $FILENAME

#sudo mutt -s “Scan from Epson 1260 Office” -a $FILENAME -c scanner@xrmx.co.uk < MSG # MSG contains a simple message.


#echo 0 > /sys/class/gpio/gpio18/value # job completed – Turn RED led off.
