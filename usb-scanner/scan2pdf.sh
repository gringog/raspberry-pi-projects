#!/bin/bash

FILENAME=`date +%s`.pdf
TMPFILE_PS="/tmp/scan.ps"

sudo scanimage --resolution 300 -x 210 -y 297 | pnmtops -width=8.27 -height=11.69 > $TMPFILE_PS
sudo ps2pdf $TMPFILE_PS $FILENAME
