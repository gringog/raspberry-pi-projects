#!/bin/bash

#initiate PINS 14 and 18 on GPIO
echo 14 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio14/direction
echo 18 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio18/direction
#turns leds 14 and 18 off
echo 0 > /sys/class/gpio/gpio14/value
echo 0 > /sys/class/gpio/gpio18/value

#bash script to initiate blinking status
blinking.sh &
# python script to listen for the push button
script_listen_for_scan.py &