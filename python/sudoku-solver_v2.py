# Source
#
# Python Sudoku Solver - Computerphile
# Youtube video https://www.youtube.com/watch?v=G_UYXzGuqvM
#
# Using backtracking and recursive functions
#

import numpy as np

grid = [[5,3,0,0,7,0,0,0,0],
        [6,0,0,1,9,5,0,0,0],
        [0,9,8,0,0,0,0,6,0],
        [8,0,0,0,6,0,0,0,3],
        [4,0,0,8,0,3,0,0,1],
        [7,0,0,0,2,0,0,0,6],
        [0,6,0,0,0,0,2,8,0],
        [0,0,0,4,1,9,0,0,5],
        [0,0,0,0,8,0,0,7,9]]


def possible(y,x,n) : 
	global grid
	# Check row
	for i in range(0,9) : 
		if grid[y][i] == n : 
			return False
	# Check column
	for i in range (0,9) : 
		if grid[i][x] == n : 
			return False
	# Check square
	x0 = (x//3)*3
	y0 = (y//3)*3
	for i in range(0,3) : 
		for j in range(0,3) : 
			if grid[y0+1][x0+j] == n : 
				return False
	return True

def solve() : 
	global grid
	for y in range(9) : 
		for x in range(9) : 
			if grid[y][x] == 0 :
				for n in range(1,10) : # Loop < 10
					if possible(y,x,n) :
						grid[y][x] = n
						solve()
						# get into a dead end, backtrack
						grid[y][x] = 0
				return
	print(np.matrix(grid))
	input("More?")

	
