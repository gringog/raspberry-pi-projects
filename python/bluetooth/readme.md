Source : http://www.raspberrypi-spy.co.uk/2013/02/nintendo-wii-remote-python-and-the-raspberry-pi/

I was looking for a way to send data to my Raspberry Pi via Bluetooth. Ideally I wanted to do this via an Android tablet and a custom app but I couldn’t get the app to connect.

During a large amount of Google searching I came across a Python library called “CWiid”. It allows you to read data from a Nintendo Wii Controller via Bluetooth and use this within your own Python script.

So I decided to give it a go and see how easy it was. As it turned out it is not difficult at all once you know the steps to take. I used the latest Raspbian image and an “Origo” USB Bluetooth dongle.


Button Mapping


Nintendo Wii Remote Button Mapping

Before starting with the practical stuff we need to look at the button mapping of the Wii remote. When you press a button on the Wii Remote it sends some data via the Bluetooth connection.

The data sent is a number that is the sum of the unique codes associated with each button.

The codes are shown in the diagram to the right and the table below in both decimal and binary notations. You will notice that each button sets a bit to “1″ in the binary number.

Decimal
 Number       Binary       Button
=================================
     1    00000000000001   2
     2    00000000000010   1
     4    00000000000100   B
     8    00000000001000   A
    16    00000000010000   MINUS
   128    00000010000000   HOME
   256    00000100000000   DOWN
   512    00001000000000   UP
  1024    00010000000000   RIGHT
  2048    00100000000000   LEFT
  4096    01000000000000   PLUS
=================================
So pressing A will send 8. Pressing LEFT will send 2048. Pressing MINUS and A at the same time will send 8 + 16 which is 24. The example script you will see further down this page receives this number and works out what buttons are being pressed.

Step 1 – Update Raspbian

I downloaded the latest Raspbian image for my testing and created a fresh SD card. To ensure I had the latest updates I ran the following two commands first :

sudo apt-get update
andsudo apt-get upgrade

These steps may take a while to complete so go and make a cup of tea while you wait.

Step 2 – Install Bluetooth Drivers

In order to communicate with Bluetooth devices (such as the Wii remote) you need to install the drivers. So start off by typing the following command :


sudo apt-get install --no-install-recommends bluetooth
The “–no-install-recommends” ensures we just install the bits we need for basic Bluetooth communication and not a load of other stuff such as printer drivers.

Step 3 – Install the CWiid Module

Next we need the CWiid Python library so type this :


sudo apt-get install python-cwiid
Step 4 – Test the Connection

Plug in your Bluetooth dongle. For devices that should work with the Pi see the hardware list on the eLinux Pi Wiki. Give it 10 seconds to power-up.

At this point it is a good idea to test that the Pi can see the Nintendo Wii remote. Type the following command :

sudo service bluetooth status
and you should get a response saying :


[ ok ] bluetooth is running.
If you don’t see this message you should reboot your Pi and try it again.

Press the 1 and 2 buttons on your Wii controller at the same time. The blue LEDs should flash. Now type :


hcitool scan
If things are going well you should see a response that shows your Pi can see your remote :


Scanning ...
         00:19:1C:B6:BB:41       Nintendo RVL-CNT-01


You can then run the script using :

python wii_remote.py
Pressing the buttons on the Wii controller should result in text being printed to the screen. That text should tell you which button was pressed. Pressing Plus and Minus together will quit the script.

What you do with your new Wii Remote powers is up to your imagination. For me I am going to use it to control a cheap radio controlled car.

