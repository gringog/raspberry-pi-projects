# Go to the user's home directory
cd ~
# Get the tar file from amazon s3 server
wget https://s3.amazonaws.com/assets.minecraft.net/pi/minecraft-pi-0.1.1.tar.gz
# Extract the compressed archive
tar -zxvf minecraft-pi-0.1.1.tar.gz
# Change directory into the mcpi folder
cd mcpi
# Echo a nice message to the user
echo "Type './minecraft-pi' without quotes to run minecraft"
