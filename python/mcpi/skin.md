Srouce : http://mcpionline.wordpress.com/tag/minecraft-pi-edition/

Using the default ‘Steve‘ skin can be boring. Why not change it up a little? Follow this guide and find out how to add your own, personal skin to your MCPI game!

# Head to the /mcpi/data/images/mob dictionary. Inside you will find a .png file called ‘char‘ (char.png). 
## Delete it or move it to another location in case you decide you prefer the default ‘Steve‘ skin.
# Copy a skin from your PC onto your Raspberry Pi using a USB Storage Device.
# On your RPi, copy or move the new skin into the /mcpi/data/images/mob dictionary.
# Rename the new skin to char.png. This allows the game to register it as the player’s skin.
# Load up MCPI and start looking cooler!

If this guide worked for you please leave a like! If you are having problems with this or any other tutorial on the site, please leave a comment!