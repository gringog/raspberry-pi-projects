import os, psutil

# Examples of psutil from http://code.google.com/p/psutil/

# cpu
print 'Cpu'
p = psutil.Process(os.getpid())
print p.get_cpu_times()
'''cputimes(user=0.06, system=0.03)'''
print p.get_cpu_percent(interval=1)

for x in range(3):
	print psutil.cpu_percent(interval=1)
	
# memory
print 'Memory'
print psutil.swap_memory()
print psutil.virtual_memory()

# Disks
print 'Disks'
print psutil.disk_partitions()
print psutil.disk_usage('/')

#network 
print 'Network'
print psutil.network_io_counters(pernic=True)
