#find out which channels are congested
sudo iwlist wlan0 scan | grep Frequency | sort | uniq -c | sort -n

#find out which channels your WiFi adaptor supports.
iwlist wlan0 channel


# wavemon is an ncurses-based monitoring application for wireless network device

# compile from source
sudo apt-get install libncurses5-dev
git clone  git://eden-feed.erg.abdn.ac.uk/wavemon.git
cd wavemon
./configure
make
sudo make install

# or install via package manager
sudo apt-get install wavemon


#  iw utility.
 sudo apt-get install iw
 iw list # shows details about the adaptor
 
 
 