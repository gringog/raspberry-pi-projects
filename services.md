
Removing Unwanted Startup Files or Services in Debian
(A) rcconf

This tool configures system services in connection with system runlevels. It turns on/off services using the scripts in /etc/init.d/. Rcconf works with System-V style runlevel configuration. It is a TUI(Text User Interface) frontend to the update-rc.d command. 

Install rcconf in Debian

#apt-get install rcconf

To start rconf, login as root user and type rcconf

# rcconf

Select the service you would like to enable or disable.



# Removing a service manually

# update-rc.d -f apache2 remove

# Adding a service manaually
# update-rc.d apache2 enable