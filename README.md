Raspberry Pi Projects collection by Greg Colley

https://bitbucket.org/gringog/raspberry-pi-projects

Custom scripts and tests collected and created from raspberry pi ideas
aimed at helping first time users to get to grips with the raspberry pi

Also for my own referance and a nice place to backup your project files thanks
to bitbuket :)

Comments or suggestions welcome via twitter @GoryGreg #rpi_projects
