Source : https://github.com/Hexxeh/rpi-update

Requirements 
============
# Install the ca-certificats
sudo apt-get install ca-certificates

# Make sure your time is correct on the raspberry pi
sudo ntpdate -u ntp.ubuntu.com

Installing
==========

sudo wget http://goo.gl/1BOfJ -O /usr/bin/rpi-update && sudo chmod +x /usr/bin/rpi-update

Updating 
========
sudo rpi-update

Activating 
==========
After the firmware has been sucessfully updated, you'll need to reboot to load the new firmware.