Source : http://n00blab.com/cacti-pi/

apt-get install nginx php5-mysql php5-snmp rrdtool snmp snmpd mysq-client mysql-server

# using nginx so lets go to the web root
cd /usr/share/nginx/www

## Download cacti
#wget http://www.cacti.net/downloads/cacti-0.8.8a.tar.gz
#tar xzvf cacti-0.8.8a.tar.gz
#mv cacti-0.8.8a cacti

#or checkot wth svn 
apt-get install subversion
svn checkout svn://svn.cacti.net/cacti

# Setup the db
cd cacti
mysqladmin --user=root -p create cacti
mysql --user=root -p cacti < cacti.sql

mysql --user=root -p mysql
mysql> GRANT ALL ON cacti.* TO cactiuser@localhost IDENTIFIED BY 'cacti';
mysql> flush privileges;



Edit include/config.php and specify the database type, name, host, user and password for your Cacti configuration.

vi include/config.php

$database_type = "mysql";
$database_default = "cacti";
$database_hostname = "localhost";
$database_username = "cactiuser";
$database_password = "cacti";

Set the appropriate permissions on cacti’s directories for graph/log generation. You should execute these commands from inside cacti’s directory to change the permissions.

shell> chown -R www-data rra/ log/

Add a line to your crontab file:

vi /etc/crontab

*/5 * * * * www-data php /var/www/cacti/poller.php > /dev/null 2>&1

Restart apache and mysql:

service apache2 restart

service mysql restart

Point your web browser to:

http://your-server/cacti/


Log in the with a username/password of admin. You will be required to change this password immediately. Make sure all of the path variables show found on the following screen.

At this point you should have an operational install up and running with the default localhost being monitored for basic system info. In order to gather more info including processor and interface traffic edit the following:

cd /etc/snmp

cp snmpd.conf snmpd.conf_orig

vi snmpd.conf

Uncomment and edit the following:

#rocommunity secret 10.0.0.0/16

to:

rocommunity cacti

Then restart snmpd

service snmpd restart

You can now edit the host within cacti under console > device to allow for the use of snmp and add the snmp processor and interface data queries.