#!/usr/bin/env python2.7

####################################
#
# program to make leds "breath" via intensity variation.
# Largely inspired by GB_Python examples by Alex Eames 
#
# Algorith copied from verilog sample file that comes with 
# DE0_NANO board (Terasic Technologies Inc.)
#
# Source : http://www.raspberrypi.org/phpBB3/viewtopic.php?f=42&t=38083
#
####################################
import RPi.GPIO as GPIO
import time
import sys
GPIO.setmode(GPIO.BCM)                                  # initialise RPi.GPIO


#
# Rev2 gertboard
#
# ports = [25, 24, 23, 22, 27, 18, 17, 11, 10, 9, 8, 7]
BUF1=25
BUF2=24
BUF3=23 
BUF4=22
BUF5=27
BUF6=18
BUF7=17
BUF8=11
BUF9=10
BUF10=9
BUF11=8
BUF12=7
#
# Trace the gertboard  BUF signal(for LED feedback)
#
outputs=[BUF3, BUF4, BUF5, BUF6, BUF7,BUF8, BUF9,BUF10, BUF11, BUF12]


###################
# set the outputs
###################
for i in outputs:
    print " setting in output:" + str(i)
    GPIO.setup(i, GPIO.OUT,initial=GPIO.LOW)                             # set up outputs
#
# limit the output as  IO access is slow
#

outputs=[ BUF5, BUF6, BUF7,BUF8, BUF9, BUF10]
counter=0
PWM_adj=0
PWM_width=0
led_val=0
shift_count= 11
shift_PWM_adj = shift_count-1
shift_PWM_width = 4
print "trial breath_led based on DE0_NANO verilog file"
raw_input("When ready hit enter.\n")

try:
#####################################
# MAIN LOOP 
######################################
   while True:
      counter = counter + 1
      PWM_width = (PWM_width & 0x2f)+ PWM_adj ;
      if ((counter  >> shift_count) & 1)==1:
#         sys.stdout.write ("adjmin")
         PWM_adj= (counter  >> shift_PWM_adj)
      else:
         PWM_adj= (~(counter  >> shift_PWM_adj)) 


      ##############################################
      # we want to drive the led with a bit of PWM_width
      #
      ##############################################
      led_val= int((PWM_width >> shift_PWM_width ) & 1)
      for out_id2 in outputs:
         #echo "out id:$out_id2"
         GPIO.output(out_id2, led_val )


except KeyboardInterrupt:                   # trap a CTRL+C keyboard interrupt
   sys.stdout.write ("\n\nCTRL-C pressed:cleaning up... \n\n")
   GPIO.cleanup()    
   # clean up GPIO ports on CTRL+C
GPIO.cleanup()                             # on finishing, reset all GPIO ports used by this program
