Source : http://jeffskinnerbox.wordpress.com/2012/11/26/speech-synthesis-on-the-raspberry-pi/
Speech Synthesis on the Raspberry Pi
======================================

Now that I can get sound out of my Raspberry Pi (RPi), the next logical step for me is speech synthesis … Right?  I foresee my RPi being used as a controller/gateway for other devices (e.g. RPi or Arduino).  In that capacity, I want the RPi to provide status via email, SMS, web updates, and so why not speech?  Therefore, I’m looking for a good text-to-speech tool that will work nicely with my RPi.

The two dominate free speech synthesis tools for Linux are eSpeak and Festival (which has a light-weight version called Flite). Both tools appear very popular, well supported, and produce quality voices.  I sensed that Festival is more feature reach and configurable, so I went with it.

To install Festival and Flite (which doesn’t require Festival to be installed), use the following command:

sudo apt-get install festival
sudo apt-get install flite

Festival
========
To test out the installation, check out Festival’s man page, and execute the following:

echo "Why are you in front of the computer?" | festival --tts
date '+%A, %B %e, %Y' | festival --tts
festival --tts Gettysburg_Address.txt

Text, WAV, Mp3 Utilities
========================
Festival also supplies a tool for converting text files into WAV files.  This tool, called text2wave can be executes as follows:

text2wave -o HAL.wav HAL.txt
aplay HAL.wav

MP3 files can be 5 to 10 times smaller than WAV files, so it might be nice to convert a WAV file to MP3.  You can do this via a tool called lame.

lame HAL.wav HAL.mp3



Flite
=====
Flite (festival-lite) is a small, fast run-time synthesis engine developed using Festival for small embedded machines. Taking a famous quote from HAL, the computer in the movie “2001: A Space Odyssey“

flite "Look Dave, I can see you're really upset about this. I honestly think you ought to sit down calmly, take a stress pill, and think things over."

Depending how the software was built for the package, you find that flite (and festival) has multiple voices. To find what voices where built in, use the command

flite -lv

To play a specific voice from the list, use the -voice parameter in the command

flite -voice kal "I'm now speaking kal's voice. By the way, please call me Dr. Hawking."

In my case, the default voice appears to be “kal”, which sounds somewhat like Stephen Hawking.  ”slt” appears to be a female version of the “kal” voice.

flite_time is a talking clock that can speak things like “The time is now, exactly two, in the afternoon.”

flite_time 14:00
flite_time `date +%H:%M`