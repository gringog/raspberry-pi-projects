Source : http://jeffskinnerbox.wordpress.com/2012/11/15/getting-audio-out-working-on-the-raspberry-pi/

To get things going, I installed ALSA, a MP3 tools, and a WAV to MP3 conversion tool via the following commands:

sudo apt-get install alsa-utils
sudo apt-get install mpg321
sudo apt-get install lame

Enabling the Sound Module
=========================
Reboot the RP and when it comes back up, its time to load the Sound Drivers.  This will be done via loadable kernel module (LKM) which are object file  that contains code to extend the Linux kernel.  lsmod is a command on Linux systems which prints the contents of the /proc/modules file.  It shows which loadable kernel modules are currently loaded.  In my case, lsmod gives me:

Module                  Size  Used by
snd_bcm2835            12808  0
snd_pcm                74834  1 snd_bcm2835
snd_seq                52536  0
snd_timer              19698  2 snd_seq,snd_pcm
snd_seq_device          6300  1 snd_seq
snd                    52489  5 snd_seq_device,snd_timer,snd_seq,snd_pcm,snd_bcm2835
snd_page_alloc          4951  1 snd_pcm


The snd-bcm2835 module appears to be already installed. RPi has a Broadcom  BCM2835 system on a chip (SoC) which is a High Definition 1080p Embedded Multimedia Applications Processor.  snd-bcm2835 is the sound driver.  If  lsmod doesn’t list the snd-bcn2835 module, then it can be installed via the following command:

sudo modprobe snd-bcm2835

Enabling Audio Output
=====================
By default, the RPi audio output is set to automatically select the digital HDMI interface if its being used, otherwise the analog audio output. You can force it to use a specific interface via the sound mixer controls.  amixer allows command-line control of the mixer for the ALSA driver.

You can force the RPi to use a specific interface using the command amixer cset numid=3 N where the N parameter means the following: 0=auto, 1=analog, 2=hdmi.  Therefore, to force the Raspberry Pi to use the analog output:

amixer cset numid=3 1

Sound Check
===========
aplay numnuts.wav
speaker-test -t sine -f 440 -c 2 -s 1
mpg321 "Mannish Boy.mp3"


Volume Control
==============
The RPi built-in sound chips don’t have a “master volume” control, and as a result, you must control the volume via software.  I guess the RPi views itself as a preamplifier (preamp) and volume controls would be supplied down stream.  ALSA provides a software volume control using the softvol plugin.

The /etc/asound.conf file is a configuration files for ALSA drivers (system-wide).  The main use of this configuration file is to add functionality. It allows you to create “virtual devices” that pre or post-process audio streams. Any properly written ALSA program can use these virtual devices as though they were normal devices.  My RPi  /etc/asound.conf file looks like this:

For most changes to /etc/asound.conf you will need to restart the sound server (ie. sudo /etc/init.d/alsa-utils restart) for the changes to take effect.

I attempted to implement the software volume controls outline in a softvol how-to that I found, but I couldn’t get it to work.  I did some additional digging, and I found a solution buried with a python script for a Adafruit project.  The following works for controlling the volume (in this case, reducing the volume to 80% of maximum):

amixer cset numid=1 -- 80%

Note that you can use this command to change the volume while sound is being played an its effect takes place immediately.  Also, I noticed that once the volume has been adjusted, its effect remains even after a reboot


WAV and MP3 Conversion
========================
Th MP3 player mpg321 can convert MP3 files to WAV files but the WAV player, aplay, can not do a conversion.  To make a MP3 file from a WAV file, you’ll need the tool lame.

To convert from WAV to MP3: lame input.wav output.mp3
To convert from MP3 to WAV:  mpg321 -w output.wav input.mp3

Bottomeline
===========
While you can get ALSA working on the Raspberry Pi, it appears only partly supported, maybe buggy, and poorly documented.  If you just want to simply get sound out of the device (like I do), you’ll be fine.  But if you have some desire to do some sound processing with ALSA, your likely to be very frustrated.
