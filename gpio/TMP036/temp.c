#include <stdlib.h>
#include "gb_common.h"
#include "gb_spi.h"

// For A to D we only need the SPI bus and SPI chip select A
void setup_gpio()
{
   INP_GPIO(8);  SET_GPIO_ALT(8,0);
   INP_GPIO(9);  SET_GPIO_ALT(9,0);
   INP_GPIO(10); SET_GPIO_ALT(10,0);
   INP_GPIO(11); SET_GPIO_ALT(11,0);
} // setup_gpio


//
//  Read TMP036 temperature via ADC input 0/1
//
int main(int argc, char* argv[])
{
  int r, v;
  int chan = -1;
  double vref = 3.3;
  double temp = 0.0;
  double volts = 0.0;
  
  if (argc < 2 || argc > 3) {
    printf("usage: tmp036 channel [Vcc]\n");
    exit(-1);
  }

  chan = atoi(argv[1]);
  printf("Channel = %d\n", chan);

  if (chan < 0 || chan > 1) {
    printf("Channel must be 0 or 1\n");
    exit(1);
  }

  if (argc == 3) {
    vref = atof(argv[2]);
  }

  printf("Vref = %f\n", vref);

  // Map the I/O sections
  printf("Setup IO\n");
  setup_io();

  // activate SPI bus pins
  printf("Setup GPIO\n");
  setup_gpio();

  // Setup SPI bus
  printf("Setup SPI\n");
  setup_spi();

  for (r=0; r<100000; r++)
  {
    v = read_adc(chan);
    // V should be in range 0-1023

    volts = (vref * (double)v / 1023.0) * 1000.0;
    temp = ((vref * (double)v / 1023.0) - 0.5) * 100.0;  // TMP036 has 500mV offset

    printf("%04d\t", v);
    printf("%4.2f mV\t", volts);
    printf("%4.2f C", temp);

    putchar(0x0D); // go to start of the line
    short_wait();
  } // repeated read

  printf("\n");
  restore_io();
} // main