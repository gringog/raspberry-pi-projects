#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include "gb_common.h"
#include "gb_spi.h"

/*

Source : http://www.raspberrypi.org/phpBB3/viewtopic.php?f=42&t=32107

I tried the sampling at maximum speed with no delay and then with several seconds between samples using the direct TMP036 to ADC connection. The result was the same under-reading no matter the sample period. I tried Gert's solution with a capacitor and even a 0.1uF decoupling capacitor brought the value up to the expected reading.

I've attached the updated code that loops continously until ctrl-C is used to halt it. The program takes 3 parameters:

sudo ./tmp036 channel [delay] [vcc]

channel: ADC channel 0/1
delay: optional delay between samples in seconds, accepts decimals e.g. 0.5. Defaults to 1s.
vcc: optional positive voltage rail level of your Gertboard provided to your ADC which is used as the reference for comparision. Defaults to 3.3v.

My Vcc is sightly higher than 3.3v at 3.325v and this makes a difference to the TMP036 reading as it's sensitive to small differences in the voltage as it's calibrated at 10mV/C.

e.g. to sample channel 0 every 1/2 second with a 3.325v reference use: sudo ./tmp036 0 0.5 3.325

*/
// For A to D we only need the SPI bus and SPI chip select A
void setup_gpio()
{
   INP_GPIO(8);  SET_GPIO_ALT(8,0);
   INP_GPIO(9);  SET_GPIO_ALT(9,0);
   INP_GPIO(10); SET_GPIO_ALT(10,0);
   INP_GPIO(11); SET_GPIO_ALT(11,0);
} // setup_gpio

// Catch ctrl-c to kill program but run restore_io() first
void int_handler(int sig) {
  restore_io;
  printf("Restore IO\n");
  exit(0);
}

//
//  Read TMP036 temperature via ADC input 0 
//
int main(int argc, char* argv[])
{
  int r, v;
  int chan = -1;
  int delay = 1000000;
  double vref = 3.3;
  double temp = 0.0;
  double volts = 0.0;

  signal(SIGINT, int_handler); // Handler for ctrl-C
  
  if (argc < 2 || argc > 4) {
    printf("usage: tmp036 channel [delay] [Vcc]\n");
    exit(-1);
  }

  chan = atoi(argv[1]);
  printf("Channel = %d\n", chan);

  if (chan < 0 || chan > 1) {
    printf("Channel must be 0 or 1\n");
    exit(1);
  }

  if (argc > 2) {
    delay = 1000000.0 * atof(argv[2]);
  }

  printf("Delay = %fs\n", (double)delay / 1000000.0);

  if (argc > 3) {
    vref = atof(argv[3]);
  }

  printf("Vref = %f\n", vref);

  // Map the I/O sections
  printf("Setup IO\n");
  setup_io();

  // activate SPI bus pins
  printf("Setup GPIO\n");
  setup_gpio();

  // Setup SPI bus
  printf("Setup SPI\n");
  setup_spi();

  for (;1;)
  {
    v = read_adc(chan);

    volts = (vref * (double)v / 1023.0) * 1000.0;
    temp = ((vref * (double)v / 1023.0) - 0.5) * 100.0;  // TMP036 has 500mV offset

    printf("%04d\t", v);
    printf("%4.2f mV\t", volts);
    printf("%4.2f C", temp);
    printf("\n");
    fflush(stdout);
 
    usleep(delay);
  } // repeated read

  printf("\n");
  restore_io();
} // main