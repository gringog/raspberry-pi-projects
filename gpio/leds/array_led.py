from array import *
from time import sleep
import RPi.GPIO as GPIO
# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BCM)

GPIO_pins = array('i',[2,3,4,5,6,7,8,9])

# Loop and setup the GPIO pins to output mode
for i in GPIO_pins:
	GPIO.setup(i, GPIO.OUT)
	print(i)

# Set the putputs to true and falsh in a loop
while 1:
	
	# Check for q keypress and quite out of the loop
	
	# Loop around the GPIO_pins array and turn them on and off.
	for i in GPIO_pins:
		GPIO.output(i, True)
		sleep(0.05)
		GPIO.output(i, False)
		sleep(0.05)
		print(i)
	# reverse the array
	GPIO_pins.reverse()
	
	
	