from time import sleep
import RPi.GPIO as GPIO
GPIO.setwarnings(False)


# choose BOARD or BCM  
GPIO.setmode(GPIO.BCM)               # BCM for GPIO numbering  
GPIO.setmode(GPIO.BOARD)             # BOARD for P1 pin numbering  
  
GPIO.setup(7, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)


GPIO.output(7, False)
GPIO.output(11, False)
GPIO.output(12, False)
GPIO.output(13, False)
GPIO.output(15, False)
GPIO.output(16, False)
GPIO.output(18, False)
GPIO.output(22, False)

	 
