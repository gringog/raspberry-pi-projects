# Source : http://pihw.wordpress.com/meltwaters-pi-hardware-kits/rgb-led-kit/rgb-led-lesson-1-creating-python-libraries-colours/

def led_setup():
 #Set up the wiring
 GPIO.setmode(GPIO.BOARD)
 # Setup Ports
 for val in LED:
   GPIO.setup(val, GPIO.OUT)
 for val in RGB:
   GPIO.setup(val, GPIO.OUT)
 led_clear()
…
…
def led_cleanup():
 led_clear()
 GPIO.cleanup()

def main():
 led_setup()
 led_activate(LED1,RGB_RED)
 time.sleep(5)
 led_deactivate(LED1,RGB_RED)
 led_cleanup()