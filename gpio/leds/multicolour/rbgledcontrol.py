#!/usr/bin/python
# Source : http://pihw.wordpress.com/meltwaters-pi-hardware-kits/rgb-led-kit/rgb-led-lesson-1-creating-python-libraries-colours/
# rgbledcontrol.py
# Test program to show the use of libraries
import time
import rgbled as RGBLED

def main():
  print "Setup the RGB module"
  RGBLED.led_setup()
  print "Switch on LED1 RED"
  RGBLED.led_activate(RGBLED.LED1,RGBLED.RGB_RED)
  time.sleep(5)
  RGBLED.led_deactivate(RGBLED.LED1,RGBLED.RGB_RED)

  print "Finished"
  RGBLED.led_cleanup()

main()
#End