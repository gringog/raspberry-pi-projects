import RPi.GPIO as GPIO
import time

# Set up header pin 11 as an input
print "Setup Pin 11"
# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)

var=1
print "Start loop"
while var==1 :
  print "Set Output False"
  GPIO.output(11, False)
  time.sleep(1)
  print "Set Output True"
  GPIO.output(11, True)
  time.sleep(1)

