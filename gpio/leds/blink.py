from time import sleep
import RPi.GPIO as GPIO
# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
while 1:
	GPIO.output(7, False)
	sleep(1)
	GPIO.output(7, True)
	sleep(1)

	GPIO.output(11, False)
	sleep(1)
	GPIO.output(11, True)
	sleep(1)

	GPIO.output(12, False)
	sleep(1)
	GPIO.output(12, True)
	sleep(1)

	GPIO.output(13, False)
	sleep(1)
	GPIO.output(13, True)
	sleep(1)
	GPIO.output(15, False)
	sleep(1)
	GPIO.output(15, True)
	sleep(1)
	
	GPIO.output(16, False)
	sleep(1)
	GPIO.output(16, True)
	sleep(1)
	
	GPIO.output(18, False)
	sleep(1)
	GPIO.output(18, True)
	sleep(1)
	
	GPIO.output(22, False)
	sleep(1)
	GPIO.output(22, True)
	sleep(1)
	 
