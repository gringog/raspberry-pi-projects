Soruce : http://blog.thestateofme.com/2012/08/10/raspberry-pi-gpio-joystick/

The first step was to get it hooked up to the RPi general purpose input output (GPIO). I used a breadboard with my homebrew Pi Cobbler at one end and a similar connector at the other hooked up to an old PC serial card cable that has the right (male) DB9 connector for the joystick plug (female). It was then just a matter of adding some pull up resistors (10K) and some patch cables:

Since I was originally planning to use gpio-keys I used the joystick pinout to hook up to the RPi thus:

1. Up -> 11 (GPIO 17)
2. Down -> 13 (GPIO 22)
3. Left -> 15 (GPIO 23)
4. Right -> 16 (GPIO 24)
5. n/c
6. Fire -> 7 (GPIO 4)
7. n/c
8. GND
9. n/c

After some digging around the Raspberry Pi Forums I found a comment about using Python to generate keystrokes. This got me headed in the direction of Python uinput, which is a module that can create keypresses.

sudo modprobe uinput
git clone https://github.com/tuomasjjrasanen/python-uinput
cd python-uinput
sudo python setup.py install --prefix=/usr/local

Digging around the examples for Python-uinput I found one for joystick, so I had a go at creating a GPIO connected variant:

With that saved as rpi-gpio-jstk.py it was a simple case of running:

sudo python rpi-gpio-jstk.py &

I tested using advj, and it showed input. When I fired up AdvMAME the joystick worked – horray – time for some gaming:)
