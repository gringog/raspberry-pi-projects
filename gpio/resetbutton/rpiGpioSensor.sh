#! /bin/bash

# Originally posted to http://www.raspberrypi.org/phpBB3/viewtopic.php?f=29&t=14255

# This script is used to check a pin at half second intervals and
# will shutdown the system if the pin is at a low level.

# Portions of this script adapted from "GPIO Driving Example (Shell script)"
# found on elinux.org .

# Note that the GPIO numbers that you program here refer to the pins
# of the BCM2835 and *not* the numbers on the pin header.
# So, if you want to activate GPIO7 on the header you should be
# using GPIO4 in this script. Likewise if you want to activate GPIO0
# on the header you should be using GPIO17 here.
# GPIO numbers should be from this list
# 0, 1, 4, 7, 8, 9, 10, 11, 14, 15, 17, 18, 21, 22, 23, 24, 25

## I'm not sure what all that means but using a 0 in this script
## indicates using the pin just next to 3v3 (the not 5v one)

# use GPIO 0 as indicated on the header
# this pin will be used to shutdown the system
PIN_ON_BCM2835="0"

READ_PIN_PATH=/sys/class/gpio/gpio"$PIN_ON_BCM2835"/value

# Set up GPIO 17 and set to input
echo "$PIN_ON_BCM2835" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio"$PIN_ON_BCM2835"/direction

# if the pin exists ...
# if the pin is low do the shutdown command with halt now.
# That is in 0 seconds.
while ( true ); do
        if [ -a "$READ_PIN_PATH" ]; then
                if [ $(<$READ_PIN_PATH) == 0 ]; then
                   shutdown -h 0
                   echo "pin is low"
                fi
        fi
# a small delay
        sleep 0.5
#  for debugging just show the pin value
#  echo $(<$READ_PIN_PATH)
done