from array import *
from time import sleep
import RPi.GPIO as GPIO
GPIO.setwarnings(False)


# choose BOARD or BCM  
GPIO.setmode(GPIO.BCM)               # BCM for GPIO numbering  
#GPIO.setmode(GPIO.BOARD)             # BOARD for P1 pin numbering  
  

GPIO_pins = array('i',[9])

# Loop and setup the GPIO pins to output mode
for i in GPIO_pins:
    GPIO.setup(i, GPIO.OUT)
	#print(i)

# Set the putputs to true and falsh in a loop
while 1:
	
	# Check for q keypress and quite out of the loop
	
	# Loop around the GPIO_pins array and turn them on and off.
	for i in GPIO_pins:
		GPIO.output(i, True)
		sleep(0.05)
		GPIO.output(i, False)
		sleep(0.05)
		
	# reverse the array
	GPIO_pins.reverse()
	
