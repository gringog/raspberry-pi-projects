import RPi.GPIO as GPIO
import time

#adjust for where your switch is connected
buttonPin = 20
GPIO.setmode(GPIO.BCM)
GPIO.setup(buttonPin,GPIO.IN)

while True:
  #assuming the script to call is long enough we can ignore bouncing
  if (GPIO.input(buttonPin)):
    #this is the script that will be called (as root)
    #os.system("python /home/pi/start.py")
    print("Button Pressed")