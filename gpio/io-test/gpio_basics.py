# source : http://raspi.tv/2013/rpi-gpio-basics-6-using-inputs-and-outputs-together-with-rpi-gpio-pull-ups-and-pull-downs

import RPi.GPIO as GPIO
import os
from time import sleep     # this lets us have a time delay (see line 12)
GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)   # set GPIO25 as input (button)
GPIO.setup(3, GPIO.OUT)   # set GPIO24 as an output (LED)

try:
    while True:            # this will carry on until you hit CTRL+C
        if GPIO.input(20): # if port 25 == 1
            print "Port 20 is 1/HIGH/True - LED ON"
            GPIO.output(3, 1)         # set port/pin value to 1/HIGH/True
            
            os.system("sudo scanimage -x 215 -y 297 --resolution 300 | convert - scan.png")
            
        else:
            print "Port 20 is 0/LOW/False - LED OFF"
            GPIO.output(3, 0)         # set port/pin value to 0/LOW/False
        sleep(0.1)         # wait 0.1 seconds


except KeyboardInterrupt:  
    # here you put any code you want to run before the program   
    # exits when you press CTRL+C  
    print "\nCTRL+C was pressed. Exiting\n"
    
except:  
    # this catches ALL other exceptions including errors.  
    # You won't get any error messages for debugging  
    # so only use it once your code is working  
    print "Other error or exception occurred!" 
    
finally:                   # this block will run no matter how the try block exits
    GPIO.cleanup()         # clean up after yourself
