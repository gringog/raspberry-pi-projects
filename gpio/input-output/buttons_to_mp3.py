#!/usr/bin/env python

# Install the audio
#$ sudo apt-get install alsa-utils
#$ sudo apt-get install mpg321

# reboot the rpi and then load the sound drives for the 5.3m jack output
#$ sudo modprobe snd-bcm2835
# You can force the RPi to use a specific interface using the command amixer cset numid=3 N where the N parameter means the following: 0=auto, 1=analog, 2=hdmi.  Therefore, to force the Raspberry Pi to use the analog output:
#$ sudo amixer cset numid=3 1 

from time import sleep
import os
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(23,GPIO.IN)
GPIO.setup(24,GPIO.IN)
GPIO.setup(25,GPIO.IN)

while True:
		if (GPIO.input(23) == True):
				os.system('mpg321 sound1.mp3 &')
		if (GPIO.input(24) == True):
				os.system('mpg321 sound2.mp3 &')
		if (GPIO.input(25) == True):
				os.system('mpg321 sound3.mp3 &')
		sleep(1);
		