#!/usr/bin/env python
import pygame
import RPi.GPIO as GPIO

pygame.init()

sound1 = pygame.mixer.Sound('../../../piboard/up.wav')
sound2 = pygame.mixer.Sound('../../../piboard/down.wav')

GPIO.setmode(GPIO.BOARD)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(11, GPIO.IN)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(12, GPIO.IN)
while 1:

	if not GPIO.input(11):
		#sound1.play()
		#print("You pressed GPIO button 11")
		GPIO.output(13, False)
	else:

		GPIO.output(13, True)
		
	if not GPIO.input(12):
		#sound2.play()
		#print("You pressed GPIO button 12")
		GPIO.output(15, False)
	else:
		GPIO.output(15, True)
		
