#!/usr/bin/python
# Source : http://pihw.wordpress.com/2013/01/30/sometimes-it-can-be-simple/
import RPi.GPIO as GPIO

#Detect and display board information
print "Meltwater's Pi Hardware - GPIO Info"
print "Board Revision: " + str(GPIO.RPI_REVISION)
print "P1:"
print " 3V3 1 2 5V"
if (GPIO.RPI_REVISION == 1):
  print " SDA0 3 4 5V"
  print " SCL0 5 6 Gnd"
else:
  print " SDA1 3 4 5V"
  print " SCL1 5 6 Gnd"
print " GPIO04 7 8 Tx"
print " Gnd 9 10 Rx"
print " GPIO17 11 12 GPIO18"
if (GPIO.RPI_REVISION == 1):
  print " GPIO21 13 14 Gnd"
else:
  print " GPIO27 13 14 Gnd"
print " GPIO22 15 16 GPIO23"
print " 3V3 17 18 GPIO24"
print " MOSI 19 20 Gnd"
print " MISO 21 22 GPIO25"
print " SCLK 23 24 CE0"
print " Gnd 25 26 CE1"
if (GPIO.RPI_REVISION != 1):
  print ""
  print "P5:"
  print " 3V3 2 1 5V"
  print " SCL0 4 3 SDA0"
  print " GPIO31 6 5 GPIO30"
  print " Gnd 8 7 Gnd"