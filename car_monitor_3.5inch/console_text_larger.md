Making the text larger on a 3.5" car monitor
============================================
Source : http://www.raspberrypi-spy.co.uk/2012/11/cheap-miniature-lcd-screen/

The text on the screen is very small and barely readable. This can be improved by increasing the font size of the console text. To do this run this command :

sudo dpkg-reconfigure console-setup

Then using the arrow and enter keys select the following options :

- Encoding to use on the console : UTF-8
- Character set to support : Guess optimal character set
- Font for the console : Change 'Do not change hte boot/kernel font to 'VGA'
- Font size : Change 8x8 to 16×28 (framebuffer only)

- Have noticed that you can change it to Terminus 6x6 (framebuffer only) and this looks quite nice but you do lose the rpi logo you also need to change your /boot/config.txt file to have the following
  framebuffer_width=350
  framebuffer_height=248

Modifying a 3.5" car display for USB Power
==========================================
http://www.raspberrypi.org/phpBB3/viewtopic.php?f=64&t=17651
http://www.wisegai.com/2012/11/29/raspberry-pi-using-a-3-5-tft-car-monitor-optional-powered-using-usb/
