
SELECT COUNT(*) FROM `wordlists`; -- 49820

SELECT COUNT(DISTINCT(`wordlist_word`)) FROM `wordlists`; -- 48961

INSERT INTO `wordlists`( word )
    SELECT DISTINCT(`wordlist_word`)
            FROM `wordlists_with_duplicates`
        ORDER BY wordlist_word ASC;