-- Login to mysql with the root user and password
-- from the bash prompt type #mysql -uroot -p

-- Create a new database:
CREATE DATABASE test;

-- Create a new user with local privileges:
CREATE USER 'pi'@'localhost' identified by 'raspberry';

-- Allow full access to the 'test' database:
GRANT ALL privileges on test.* to 'pi'@'localhost';

-- Remove permissions
REVOKE ALL ON `test`.* FROM 'pi'@'localhost';

-- Grant limited access to the test database 
GRANT SELECT,INSERT,UPDATE,DELETE ON test.* TO 'test'@'localhost';

-- See what permissions we have for this user
SHOW GRANTS FOR 'pi'@'localhost';

-- Clean up:
FLUSH PRIVILEGES;



-- Exit mysql command line
quit;