------------------------------------------------------------
-- Find Plesk domains exceeding quotas
-- Find domains that are over quota on disk space in Plesk:
------------------------------------------------------------
SELECT domains.name, domains.real_size, Limits.value
FROM domains, Limits
WHERE domains.limits_id = Limits.id
AND domains.real_size > Limits.value
AND limit_name = 'disk_space'
AND Limits.value != -1
ORDER BY domains.name ASC;

-- Domain names
SELECT domains.name
FROM domains
WHERE `name` LIKE '%.domain.com';

------------------------------------------------------------
-- FTP ACCOUNTS
------------------------------------------------------------
SELECT account_id AS ‘ID’, login AS ‘USERNAME’, PASSWORD AS ‘PASSWORD’, home AS ‘HOMEDIR’ FROM sys_users S, accounts A WHERE S.account_id = A.id;

------------------------------------------------------------
--  MAIL ACCOUNTS
------------------------------------------------------------
-- For all Domains
SELECT account_id AS ‘ID’, mail_name AS ‘USERNAME’, PASSWORD AS ‘PASSWORD’, postbox AS ‘MAILBOX?’, NAME AS ‘DOMAIN’, redir_addr AS REDIRECT 
FROM mail M, domains D, accounts A 
WHERE M.account_id = A.id AND M.dom_id = D.id 
ORDER BY NAME;


-- For a Specific Domain
SELECT account_id AS ‘ID’, mail_name AS ‘USERNAME’, PASSWORD AS ‘PASSWORD’, postbox AS ‘MAILBOX?’, NAME AS ‘DOMAIN’, redir_addr AS REDIRECT 
FROM mail M, domains D, accounts A 
WHERE M.account_id = A.id AND M.dom_id = D.id AND D.name=’****DOMAIN_NAME****‘;

------------------------------------------------------------
-- MySQL ACCOUNTS
------------------------------------------------------------
-- Plesk 7
SELECT d.name AS DOMAIN, db.name AS DB, du.login AS USER, du.passwd AS PASS 
FROM db_users du, data_bases db, domains d 
WHERE du.db_id = db.id AND db.dom_id=d.id 
ORDER BY d.name, db.name;

-- Plesk 8 & 9
SELECT d.name AS DOMAIN, db.name AS DB, du.login AS USER, a.password AS PASS 
FROM db_users du, data_bases db, domains d, accounts a 
WHERE du.db_id = db.id AND db.dom_id=d.id AND du.account_id=a.id 
ORDER BY d.name, db.name;

------------------------------------------------------------
CLIENT ACCOUNTS
------------------------------------------------------------

-- Plesk 7
SELECT id AS ‘ID’, login AS ‘USERNAME’, passwd AS ‘PASSWORD’, cname AS ‘COMPANY’ 
FROM clients  
ORDER BY login;

-- Plesk 8 & 9
SELECT c.id AS CLIENT, a.id AS account, c.login, a.password
FROM clients c, accounts a 
WHERE a.id=c.account_id;


