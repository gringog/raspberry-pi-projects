#!/bin/sh
# To Run type xinit ./kiosk_screen.sh

# Prevent the screen from blanking
# Requires : sudo apt-get install x11-xserver-utils
xset -dpms
xset s off

# Hide the mouse cursor
# Requires : sudo apt-get install unclutter
unclutter &
matchbox-window-manager & midori -e Fullscreen -a http://localhost/kiosk/
#while true; do
#       midori -e Fullscreen -a http://localhost/kiosk/
#done
