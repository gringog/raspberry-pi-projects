#!/usr/bin/env bash

# Install script for new pi's

# Upgrade the box 
sudo apt-get update
sudo apt-get upgrade

# install htop and svn
sudo apt-get install htop  subversion -fy

# AdaFruit WebIDE for the Raspberry pi
curl https://raw.github.com/adafruit/Adafruit-WebIDE/alpha/scripts/install.sh | sudo sh

# Install SickBeard CouchPotato and SABnzdb
curl https://bitbucket.org/gringog/raspberry-pi-projects/raw/d102245c47f4e1ebf9e7b8dcd7f233560ea972fa/p2p/install_p2p.sh | sudo sh

# Python psutil
sudo apt-get install python-dev -y
cd ~
svn checkout http://psutil.googlecode.com/svn/trunk/ psutil-read-only
cd ~/psutil-read-only
sudo python setup.py install


#vnc
sudo apt-get install tightvncserver
vncserver :0 -geometry 1280x800 -depth 24 -dpi 96