Samba Config file
 - Writable home directory and share the the webide

To use make a backup of your old smb.conf file::

sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.old

Then copy thie file to the /etc/samba/smb.conf locaiton::

sudo cp ./smb.conf /etc/samba/smb.conf 

Make sure you have samba installed::

sudo apt-get install samba smb-common-bin

Restart samba service if you already had samba installed::

sudo service samba restart
 
You may need to set the password for the default user this is done using a 
programm called smbpasswd. This adds the user pi and sets the password::

sudo smbpasswd -a pi

