#!/bin/bash

# Based on https://trac.ffmpeg.org/wiki/UbuntuCompilationGuide

# Get the Dependencies
sudo apt-get update
sudo apt-get -y install autoconf automake build-essential git libass-dev libgpac-dev \
  libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libx11-dev \
  libxext-dev libxfixes-dev pkg-config texi2html zlib1g-dev libmp3lame-dev libasound2-dev

mkdir ~/ffmpeg_sources

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# YSAM
cd $DIR
./compile_install_ysam.sh

# x264 
cd $DIR
./compile_install_x264.sh

# fdk-aac
cd $DIR
./compile_install_fdk-aac.sh

# libopus
cd $DIR
./compile_install_libopus.sh

# libvpx
cd $DIR
./compile_install_libvpx.sh

# ffmpeg 
cd $DIR
./compile_install_ffmpeg.sh

