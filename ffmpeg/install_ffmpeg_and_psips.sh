
#!/usr/bin/env bash


# curl https://bitbucket.org/gringog/raspberry-pi-projects/raw/be41db8557ac81cb7921e5b9425f9afac6366b52/ffmpeg/install_ffmpeg_and_psips.sh | sudo sh

# Source : http://www.raspberrypi.org/phpBB3/viewtopic.php?p=411628#p411628


sudo apt-get install libasound2-dev git build-essential autoconf libtool

function build_yasm {
    # an assembler used by x264 and ffmpeg
    cd /usr/src
    wget http://www.tortall.net/projects/yasm/releases/yasm-1.2.0.tar.gz
    tar xzvf yasm-1.2.0.tar.gz
    cd yasm-1.2.0
    ./configure
    make
    make install
}

function build_h264 {
    # h.264 video encoder
    cd /usr/src
    git clone git://git.videolan.org/x264
    cd x264
    ./configure --disable-asm --enable-shared
    make
    make install
}

function build_lame {
    # mp3 audio encoder
    cd /usr/src
    wget http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.tar.gz
    tar xzvf lame-3.99.tar.gz
    cd lame-3.99
    ./configure
    make
    make install
}

function build_faac {
    # aac encoder
    cd /usr/src
    curl -#LO http://downloads.sourceforge.net/project/faac/faac-src/faac-1.28/faac-1.28.tar.gz
    tar xzvf faac-1.28.tar.gz
    cd faac-1.28
    ./configure
    make
    make install
}

function build_ffmpeg {
    cd /usr/src/
    git clone git://source.ffmpeg.org/ffmpeg.git
    cd ffmpeg      
    ./configure --enable-shared --enable-gpl --prefix=/usr --enable-nonfree --enable-libmp3lame --enable-libfaac --enable-libx264 --enable-version3 --disable-mmx
    make
    make install
}

function configure_ldconfig {
    echo "/usr/local/lib" > /etc/ld.so.conf.d/libx264.conf
    ldconfig
}

function build_psips {
    cd /usr/src
    git clone git://github.com/AndyA/psips.git
    cd psips
    ./setup.sh && ./configure && make && make install
}
build_yasm
build_h264
build_lame
build_faac
build_ffmpeg
configure_ldconfig
build_psips