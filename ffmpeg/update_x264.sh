#!/bin/bash

cd ~/ffmpeg_sources/x264
make distclean
git pull


./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --enable-static
make
make install
make distclean
