#!/bin/bash

cd ~/ffmpeg_sources/libvpx
make clean
git pull

./configure --prefix="$HOME/ffmpeg_build" --disable-examples
make
make install
make clean