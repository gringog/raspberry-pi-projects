#!/bin/bash

rm -rf ~/ffmpeg_build ~/bin/{ffmpeg,ffprobe,ffserver,vsyasm,x264,yasm,ytasm}
sudo apt-get update
sudo apt-get -y install autoconf automake build-essential git libass-dev libgpac-dev \
   libtheora-dev libtool libvorbis-dev \
  pkg-config texi2html zlib1g-dev
  
#x264
./update_x264.sh



# fdk-aac
./update_fdk-aac.sh



# libvpx
./update_libvpx.sh


# ffmpeg
./update_ffmpeg.sh
