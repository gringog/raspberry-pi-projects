#!/bin/bash

cd ~/ffmpeg_sources/fdk-aac
make distclean
git pull

./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make
make install
make distclean